from django.db import models
from django.utils import timezone
# Create your models here.

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    marca = models.CharField(max_length=30)
    modelo = models.CharField(max_length=30)
    año = models.CharField(max_length=4)
    color = models.CharField(max_length=20)
    numeroPuertas = models.CharField(max_length=2)
    precio = models.CharField(max_length=10)
    descripcion = models.TextField()
    fechaPublicacion = models.DateTimeField(
        blank=True, null=True)

    def publish(self):
        self.fechaPublicacion = timezone.now()
        self.save()

    def __str__(self):
            return self.title


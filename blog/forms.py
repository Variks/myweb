from django import forms

from .models import Post

class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'marca', 'modelo', 'año', 'color', 'numeroPuertas', 'precio', 'descripcion')